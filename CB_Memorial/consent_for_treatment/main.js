
document
  .getElementById("fetchForm")
  .addEventListener("submit", function (event) {
    event.preventDefault();

    // Get the value from the input field
    const id = parseInt(document.getElementById("idInput").value);

    // Fetch JSON data
    fetch(`../json.js`)
      .then((response) => response.json())
      .then((data) => {
        var pname = document.getElementsByClassName("PatientName")[0];
        var address = document.getElementsByClassName("Address")[0];
        var phone = document.getElementsByClassName("Phone")[0];
        var occupation = document.getElementsByClassName("Occupation")[0];
        var relation = document.getElementsByClassName("Relation")[0];

        const patient = data.find((entry) => entry.ID === id);
        if (patient) {
          console.log(`Doctor Name for ID ${id}:`, patient.Doctor_name);
          pname.innerHTML = patient.Patient;
          address.innerHTML = patient.Address;
          phone.innerHTML = patient.Telephone;
          relation.innerHTML = patient.Realation;
          occupation.innerHTML = patient.Occupation;
        } else {
          console.error(`No data found for ID ${id}`);
        }
      })
      .catch((error) =>
        console.error(`Error fetching JSON for ID ${id}:`, error)
      );
  });

document.getElementById("print").addEventListener("click", function () {
  var style = document.createElement("style");
  style.innerHTML =
    "@media print { #idInput, #print,#fetchForm, #logo{ display: none; } }";
  document.head.appendChild(style);
  window.print();
});




//logo insert
document.getElementById('logo').addEventListener("click", function () {
   

    fetch('../hospitalInfo.js')
    .then(response => response.json())
    .then(data => {
     
      console.log('Logo 1 Path:',data);
      
      const firstHospital = data[0];
     

      const hospitalNameElement = document.getElementById('hospitalName');
      hospitalNameElement.textContent = firstHospital.hositalName;

      const hospitalLogoElement = document.getElementById('hospitalLogo');
      hospitalLogoElement.src = firstHospital.hospitalLogo;

      const hospitalAddressElement = document.getElementById('hospitalAddress');
      hospitalAddressElement.textContent = firstHospital.hospitalAddress;
  
      const phone1=document.getElementById('phone1');
      phone1.textContent = firstHospital.hospitalPhone1;

       const phone2=document.getElementById('phone2');
      phone2.textContent = firstHospital.hospitalPhone2; 
   const doctor=document.getElementById('doctorName');
   doctor.textContent=firstHospital.doctor;
    

  
    })
    .catch(error => console.error('Error fetching logo data:', error));
});