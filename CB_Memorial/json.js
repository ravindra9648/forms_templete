[ 
  {
    "Doctor_name": "John Smith",
    "Patient": "Jane Doe",
    "Address": "123 Main St",
    "Occupation": "Teacher",
    "Telephone": 1234567890,
    "Realation": "Mother",
    "ID": 1
  },
  {
    "Doctor_name": "Sarah Johnson",
    "Patient": "Michael Brown",
    "Address": "456 Elm St",
    "Occupation": "Engineer",
    "Telephone": 9876543210,
    "Realation": "Father",
    "ID": 2
  },
  {
    "Doctor_name": "Emily Davis",
    "Patient": "Olivia Wilson",
    "Address": "789 Oak St",
    "Occupation": "Nurse",
    "Telephone": 2345678901,
    "Realation": "Sister",
    "ID": 3
  },
  {
    "Doctor_name": "David Miller",
    "Patient": "Robert Taylor",
    "Address": "321 Pine St",
    "Occupation": "Lawyer",
    "Telephone": 8765432109,
    "Realation": "Brother",
    "ID": 4
  },
  {
    "Doctor_name": "Jessica Anderson",
    "Patient": "Sophia Clark",
    "Address": "654 Cedar St",
    "Occupation": "Doctor",
    "Telephone": 3456789012,
    "Realation": "Aunt",
    "ID": 5
  },
  {
    "Doctor_name": "Michael Wilson",
    "Patient": "Daniel Hill",
    "Address": "987 Birch St",
    "Occupation": "Police Officer",
    "Telephone": 7654321098,
    "Realation": "Uncle",
    "ID": 6
  },
  {
    "Doctor_name": "Laura Thompson",
    "Patient": "Isabella Turner",
    "Address": "210 Maple St",
    "Occupation": "Accountant",
    "Telephone": 4567890123,
    "Realation": "Cousin",
    "ID": 7
  },
  {
    "Doctor_name": "Christopher White",
    "Patient": "Ethan Cooper",
    "Address": "543 Walnut St",
    "Occupation": "Chef",
    "Telephone": 6543210987,
    "Realation": "Friend",
    "ID": 8
  },
  {
    "Doctor_name": "Amanda Harris",
    "Patient": "Mia Rodriguez",
    "Address": "876 Spruce St",
    "Occupation": "Artist",
    "Telephone": 5432109876,
    "Realation": "Guardian",
    "ID": 9
  }]